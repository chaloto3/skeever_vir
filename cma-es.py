import numpy as np
import os
# from src.Lab_2.quadruped.quadruped import QuadrupedBulletEnv
from environment import Environment
import copy
import only8joints as rm
import cma


# This script will run correctly assuming you have pythonpath pointing to the src directory. If you open a pycharm project inside the hw_1 file from sources then pythonpath should be set automatically
# Otherwise you can export PYTHONPATH to add the src directory in the terminal as follows: export PYTHONPATH=${PYTHONPATH}:/home/scripts/vir/hw_1



class PolicyNet():
    def __init__(self, obs_dim, act_dim):
        # Initialize your weight parameters of your policy.
        # The simplest policy to implement would be a simple affine mapping y = xw+b, where x is the input,
        # w is the weight matrix and b is the bias
        self.obs_dim = obs_dim
        self.act_dim = act_dim
        #self.act_dim = act_dim
        self.w = np.random.uniform(low=-1., high=1., size=(act_dim, obs_dim)) / np.sqrt(2)
        self.b = np.random.uniform(low=-1., high=1., size=act_dim) / np.sqrt(2)

    def set_params(self, w):
        # This function takes in a list w and maps it to your policy parameters.
        # The simplest way is to probably make an array out of the w vector and reshape it appropriately
        # print(w.shape)
        [wm, b] = np.split(w, [(self.act_dim * self.obs_dim)])
        self.w = np.reshape(wm, (self.act_dim, self.obs_dim))
        self.b = b

    def get_params(self):
        # This function returns a list w from your policy parameters. You can use numpy's flatten() function
        vector = copy.deepcopy(self.w)
        vector.flatten()
        vector = np.append(vector, self.b)
        # print(vector.shape)
        return vector

    def sigmoid_func(self, x):
        return 1 / (1 + np.exp(-x))

    def forward(self, x):
        # Performs the forward pass on your policy. Maps observation input x to action output a
        # x = np.reshape(x, (-1, 1))
        out = 2 * self.sigmoid_func((self.w @ x + self.b) / 8) - np.ones(self.act_dim)
        # print(out)
        return out


def f_wrapper(env, policy):
    def f(w):
        reward = 0
        done = False
        obs = env.reset()
        obs = rm.reduce_redundant_obs(obs)
        # Map the weight vector to your policy

        policy.set_params(w)

        while not done:
            # Get action from policy
            act = policy.forward(obs)
            act = rm.to_joint_array(act)
            # Step environment
            obs, rew, done, _ = env.step(act)
            obs = rm.reduce_redundant_obs(obs)

            reward += rew
        return reward

    return f

def f_wrapper_cma(env, policy):
    def f_cma(w):
        reward = 0
        done = False
        obs = env.reset()
        obs = rm.reduce_redundant_obs(obs)
        # Map the weight vector to your policy

        policy.set_params(w)

        while not done:
            # Get action from policy
            act = policy.forward(obs)
            act = rm.to_joint_array(act)
            # Step environment
            obs, rew, done, _ = env.step(act)
            obs = rm.reduce_redundant_obs(obs)

            reward += rew
        return -reward

    return f_cma


def my_opt(f,f_cma, w_init, iters):
    # Your optimization algorithm. Takes in an evaluation function f, and initial solution guess w_init and returns
    # parameters w_best which 'solve' the problem to some degree.
    avg_cnt = 5
    w_best = copy.deepcopy(w_init)
    r_best = 0
    sigma=0.5
    log_file = open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'training_log/cma_15_joints_walk2.txt'),'w')
    for i in range(avg_cnt):
        r_best += f(w_best)
    r_best = r_best / avg_cnt
    print(f"\nREWARD START {r_best}")
    for i in range(iters):
        es = cma.fmin(f_cma, w_best, sigma, options={'maxfevals': 30,'verb_disp':0})
        new_r=0
        for j in range(avg_cnt):
            new_r += f(es[0])
        new_r = new_r / avg_cnt
        print(f"Iteration no.{i} new_r == {new_r}; r_best = {r_best}")
        log_file.write(f"{i}, {new_r};")
        if new_r > r_best:
            r_best = new_r
            w_best = copy.deepcopy(es[0])
            np.save(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'skeevers/cma_15_joints_walk2.npy'), w_best)
            print(f"MODEL_SAVED")
    return w_best, r_best


def test(w_best, max_steps=70, animate=False):
    # Make the environment and your policy
    env = Environment(animate=animate, max_steps=max_steps)
    obs_len, joints_cnt = rm.get_param_dim()
    policy = PolicyNet(obs_len, joints_cnt)

    # Make evaluation function
    f = f_wrapper(env, policy)

    # Evaluate
    r_avg = 0
    eval_iters = 10
    for i in range(eval_iters):
        r = f(w_best)
        r_avg += r
        print(r)
    return r_avg / eval_iters


if __name__ == "__main__":
    train = False
    policy_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'skeevers/cma_8_joints_walk.npy')
    max_steps = 70
    N_training_iters = 1800
    w_best = None

    obs_len, joints_cnt = rm.get_param_dim()

    if train:
        # Make the environment and your policy
        env = Environment(animate=False, max_steps=max_steps)
        policy = PolicyNet(obs_len, joints_cnt)
        #obs dim = act dim + 8
        # Make evaluation function
        f = f_wrapper(env, policy)
        f_cma=f_wrapper_cma(env,policy)

        # Initial guess of solution
        w_init = policy.get_params()
        #w_init = np.load(policy_path)

        # Perform optimization
        w_best, r_best = my_opt(f,f_cma, w_init, N_training_iters)

        print(f"r_best: {r_best}")

        # Save policy
        np.save(policy_path , w_best)
        env.close()

    if not train:
        w_best = np.load(policy_path)
    print(f"Avg test rew: {test(w_best, max_steps=max_steps, animate=not train)}")



