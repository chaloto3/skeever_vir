import os
import numpy as np
import pybullet as p
import pybullet_data
import time


def do_nothing():
    return 0

def execute():
    for i in range(10000):
        p.stepSimulation(physicsClientId=client_ID)

client_ID = p.connect(p.GUI)

 # Set simulation world params
p.setGravity(0, 0, -9.8, physicsClientId=client_ID)
p.setRealTimeSimulation(0, physicsClientId=client_ID)
p.setAdditionalSearchPath(pybullet_data.getDataPath(), physicsClientId=client_ID)

 # Load actual robot into the world
robot = p.loadURDF(os.path.join(os.path.dirname(os.path.realpath(__file__)), "skeever.urdf"), physicsClientId=client_ID) # Quadruped
plane = p.loadURDF("plane.urdf", physicsClientId=client_ID) # Floor

lateral_friction = 100000.0
max_force = 1.6
p.changeDynamics(robot, 7, lateralFriction=lateral_friction)
p.changeDynamics(robot, 10, lateralFriction=lateral_friction)
p.changeDynamics(plane, 0, lateralFriction=100.0)

pos = [0]*18
pos[6] = 0.
pos[9] = 0.

for i in range(18):
    p.setJointMotorControl2(bodyIndex=robot,jointIndex=i,controlMode=p.POSITION_CONTROL,targetPosition=pos[i], force = max_force, physicsClientId=client_ID)
    p.changeDynamics(robot, i, lateralFriction=lateral_friction)
    
execute()
h = input()
while(1):
    p.setJointMotorControl2(bodyIndex=robot,jointIndex=2,controlMode=p.POSITION_CONTROL,targetPosition=0, force = max_force, physicsClientId=client_ID)
    execute()
    p.setJointMotorControl2(bodyIndex=robot,jointIndex=2,controlMode=p.POSITION_CONTROL,targetPosition=0.52, force = max_force, physicsClientId=client_ID)
    execute()
    p.setJointMotorControl2(bodyIndex=robot,jointIndex=2,controlMode=p.POSITION_CONTROL,targetPosition=0, force = max_force, physicsClientId=client_ID)
    execute()
    
    for i in range(2):
        p.setJointMotorControl2(bodyIndex=robot,jointIndex=4+i*3,controlMode=p.POSITION_CONTROL,targetPosition=-0.52, force = max_force, physicsClientId=client_ID)
        execute()
        p.setJointMotorControl2(bodyIndex=robot,jointIndex=4+i*3,controlMode=p.POSITION_CONTROL,targetPosition=1.04, force = max_force, physicsClientId=client_ID)
        execute()
        p.setJointMotorControl2(bodyIndex=robot,jointIndex=4+i*3,controlMode=p.POSITION_CONTROL,targetPosition=0, force = max_force, physicsClientId=client_ID)
        execute()
        
        p.setJointMotorControl2(bodyIndex=robot,jointIndex=5+i*3,controlMode=p.POSITION_CONTROL,targetPosition=0, force = max_force, physicsClientId=client_ID)
        execute()
        p.setJointMotorControl2(bodyIndex=robot,jointIndex=5+i*3,controlMode=p.POSITION_CONTROL,targetPosition=0.52, force = max_force, physicsClientId=client_ID)
        execute()
        p.setJointMotorControl2(bodyIndex=robot,jointIndex=5+i*3,controlMode=p.POSITION_CONTROL,targetPosition=0, force = max_force, physicsClientId=client_ID)
        execute()

        p.setJointMotorControl2(bodyIndex=robot,jointIndex=6+i*3,controlMode=p.POSITION_CONTROL,targetPosition=0, force = max_force, physicsClientId=client_ID)
        execute()
        p.setJointMotorControl2(bodyIndex=robot,jointIndex=6+i*3,controlMode=p.POSITION_CONTROL,targetPosition=-0.52, force = max_force, physicsClientId=client_ID)
        execute()
        p.setJointMotorControl2(bodyIndex=robot,jointIndex=6+i*3,controlMode=p.POSITION_CONTROL,targetPosition=0, force = max_force, physicsClientId=client_ID)
        execute()
