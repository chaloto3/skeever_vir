import os
import numpy as np
import pybullet as p
import pybullet_data
import time
import torch as T

class QuadrupedBulletEnv():
    def __init__(self, animate=False, max_steps=100, seed=None):
        self.animate = animate
        self.max_steps = max_steps
        self.seed = seed

        if seed is not None:
            np.random.seed(seed)
            T.manual_seed(seed)

        # Initialize simulation
        if (animate):
            self.client_ID = p.connect(p.GUI)
        else:
            self.client_ID = p.connect(p.DIRECT)
        assert self.client_ID != -1, "Physics client failed to connect"

        # Set simulation world params
        p.setGravity(0, 0, -9.8, physicsClientId=self.client_ID)
        p.setRealTimeSimulation(0, physicsClientId=self.client_ID)
        p.setAdditionalSearchPath(pybullet_data.getDataPath(), physicsClientId=self.client_ID)

        # Load actual robot into the world
        self.robot = p.loadURDF(os.path.join(os.path.dirname(os.path.realpath(__file__)), "quadruped.urdf"), physicsClientId=self.client_ID) # Quadruped
        self.plane = p.loadURDF("plane.urdf", physicsClientId=self.client_ID) # Floor

        # Input and output dimensions defined in the environment
        self.obs_dim = 20
        self.act_dim = 12

        # Limits of our joints. When using the * (multiply) operation on a list, it repeats the list that many times
        self.joints_rads_low = np.array([-0.5, -1.8, 1.4] * 4)
        self.joints_rads_high = np.array([0.5, -0.2, 2.6] * 4)
        self.joints_rads_diff = self.joints_rads_high - self.joints_rads_low

        # Various other parameters
        self.max_joint_force = 1.6
        self.target_vel = 0.4
        self.sim_steps_per_iter = 24 # The amount of simulation steps done every iteration.
        self.lateral_friction = 1.0

        for i in range(4):
           p.changeDynamics(self.robot, 3 * i + 2, lateralFriction=self.lateral_friction)
        p.changeDynamics(self.robot, -1, lateralFriction=self.lateral_friction)

    def get_obs(self):
        '''
        Returns a suite of observations of the given state of the robot. Not all of these are required.
        :return: list of lists
        '''
        # Torso
        torso_pos, torso_quat = p.getBasePositionAndOrientation(self.robot, physicsClientId=self.client_ID) # xyz and quat: x,y,z,w
        torso_vel, torso_angular_vel = p.getBaseVelocity(self.robot, physicsClientId=self.client_ID)

        # Boolean values signifying if the given leg is in contact with the floor
        ctct_leg_1 = int(len(p.getContactPoints(self.robot, self.plane, 2, -1, physicsClientId=self.client_ID)) > 0) * 2 - 1
        ctct_leg_2 = int(len(p.getContactPoints(self.robot, self.plane, 5, -1, physicsClientId=self.client_ID)) > 0) * 2 - 1
        ctct_leg_3 = int(len(p.getContactPoints(self.robot, self.plane, 8, -1, physicsClientId=self.client_ID)) > 0) * 2 - 1
        ctct_leg_4 = int(len(p.getContactPoints(self.robot, self.plane, 11, -1, physicsClientId=self.client_ID)) > 0) * 2 - 1
        contacts = [ctct_leg_1, ctct_leg_2, ctct_leg_3, ctct_leg_4]

        # Joints
        obs = p.getJointStates(self.robot, range(12), physicsClientId=self.client_ID) # pos, vel, reaction(6), prev_torque
        joint_angles = []
        joint_velocities = []
        joint_torques = []
        for o in obs:
            joint_angles.append(o[0])
            joint_velocities.append(o[1])
            joint_torques.append(o[3])
        return torso_pos, torso_quat, torso_vel, torso_angular_vel, joint_angles, joint_velocities, joint_torques, contacts

    def rads_to_norm(self, joints):
        '''
        :param joints: list or array of joint angles in radians
        :return: array of joint angles normalized to [-1,1]
        '''
        sjoints = np.array(joints)
        sjoints = ((sjoints - self.joints_rads_low) / self.joints_rads_diff) * 2 - 1
        return sjoints

    def norm_to_rads(self, action):
        '''
        :param action: list or array of normalized joint target angles (from your control policy)
        :return: array of target joint angles in radians (to be published to simulator)
        '''
        return (np.array(action) * 0.5 + 0.5) * self.joints_rads_diff + self.joints_rads_low

    def step(self, ctrl):
        '''
        Step function.
        :param ctrl: list or array of target joint angles normalized to [-1,1]
        :return: next_obs, r, done, _  . The last '_' value is left as compatability with gym envs.
        '''

        ctrl_noisy = ctrl + np.random.rand(self.act_dim).astype(np.float32) * 0.1 - 0.05

        # YOU CAN REMOVE THIS CLIP IF YOU WANT
        ctrl_clipped = np.clip(ctrl_noisy, -1, 1)

        # Scale the action to correct range and publish to simulator (this just publishes the action, doesn't step the simulation yet)
        scaled_action = self.norm_to_rads(ctrl_clipped)
        p.setJointMotorControlArray(bodyUniqueId=self.robot,
                                    jointIndices=range(12),
                                    controlMode=p.POSITION_CONTROL,
                                    targetPositions=scaled_action,
                                    forces=[self.max_joint_force] * 12,
                                    positionGains=[0.02] * 12,
                                    velocityGains=[0.1] * 12,
                                    physicsClientId=self.client_ID)

        # Step the simulation.
        for i in range(self.sim_steps_per_iter):
            p.stepSimulation(physicsClientId=self.client_ID)
            if self.animate : time.sleep(0.004)

        # Get new observations (Note, not all of these are required and used)
        torso_pos, torso_quat, torso_vel, torso_angular_vel, joint_angles, joint_velocities, joint_torques, contacts = self.get_obs()
        xd, yd, zd = torso_vel # Unpack for clarity
        roll, pitch, yaw = p.getEulerFromQuaternion(torso_quat)

        # Calculate reward of the agent for traveling a self.target_vel velocity in the x direction (you can use your own proxy here!)
        #velocity_rew = 1. / (abs(xd - self.target_vel) + 1.) - 1. / (self.target_vel + 1.)
        velocity_rew = np.minimum(xd, self.target_vel) / self.target_vel

        # Add various rewards here to your liking. For example, np.square(yaw) * 0.5 will penalise the robot facing directions other than the x direction
        # You can use other similar heuristics to 'guide' the agent into doing what you actually want it to do.
        r_neg = np.square(yaw) * 0.5 + \
                np.square(roll) * 0.3 + \
                np.square(pitch) * 0.3 + \
                np.square(zd) * 1.
        r_pos = (velocity_rew * 1.0) / self.max_steps * 100
        r = np.clip(r_pos - r_neg, -3, 3)

        # Scale joint angles and make the policy observation
        scaled_joint_angles = self.rads_to_norm(joint_angles)
        env_obs = np.concatenate((scaled_joint_angles, torso_quat, contacts)).astype(np.float32)

        self.step_ctr += 1

        # This condition terminates the episode
        done = self.step_ctr > self.max_steps or np.abs(roll) > 1.57 or np.abs(pitch) > 1.57

        env_obs_noisy = env_obs + np.random.rand(self.obs_dim).astype(np.float32) * 0.1 - 0.05

        return env_obs_noisy, r, done, {}

    def reset(self):
        self.step_ctr = 0 # Counts the amount of steps done in the current episode

        # Reset the robot to initial position and orientation and null the motors
        joint_init_pos_list = self.norm_to_rads([0] * 12)
        [p.resetJointState(self.robot, i, joint_init_pos_list[i], 0, physicsClientId=self.client_ID) for i in range(12)]
        p.resetBasePositionAndOrientation(self.robot, [0, 0, .15], [0, 0, 0, 1], physicsClientId=self.client_ID)
        p.setJointMotorControlArray(bodyUniqueId=self.robot,
                                    jointIndices=range(12),
                                    controlMode=p.POSITION_CONTROL,
                                    targetPositions=[0] * 12,
                                    forces=[self.max_joint_force] * 12,
                                    physicsClientId=self.client_ID)

        # Step a few times so stuff settles down
        for i in range(10):
            p.stepSimulation(physicsClientId=self.client_ID)

        # Return initial obs
        obs, _, _, _ = self.step(np.zeros(self.act_dim))
        return obs

    def demo(self):
        self.reset()
        n_rep = 20
        acts = [0, 0, 0], [0, -1, -1], [0, 1, 1], [1, 0, 0], [-1, 0, 0]
        while True:
            for a in acts:
                for j in range(n_rep):
                    obs, rew, done, _ = self.step(a*4)
                    print(len(obs))
                    print(rew)
                    print(done)

    def close(self):
        p.disconnect(physicsClientId=self.client_ID)

if __name__ == "__main__":
    env = QuadrupedBulletEnv(animate=True)
    env.demo()
