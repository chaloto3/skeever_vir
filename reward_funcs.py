import os
import numpy as np
import pybullet as p
import pybullet_data
import time
import torch as T


def get_reward(torso_pos, torso_quat, torso_vel, torso_angular_vel, joint_angles, joint_velocities, joint_torques,
               contacts, target_vel, max_steps):
    xd, yd, zd = torso_vel  # Unpack for clarity
    roll, pitch, yaw = p.getEulerFromQuaternion(torso_quat)

    # Calculate reward of the agent for traveling a self.target_vel velocity in the y direction (you can use your own proxy here!)
    # velocity_rew = 1. / (abs(yd - self.target_vel) + 1.) - 1. / (self.target_vel + 1.)
    velocity_rew = np.minimum(yd, target_vel) / target_vel

    # Add various rewards here to your liking. For example, np.square(yaw) * 0.5 will penalise the robot facing directions other than the x direction
    # You can use other similar heuristics to 'guide' the agent into doing what you actually want it to do.
    r_neg = np.square(yaw) * 0.5 + \
            np.square(roll) * 0.3 + \
            np.square(pitch) * 0.3 + \
            np.square(zd) * 1.
    r_pos = (velocity_rew * 1.0) / max_steps * 100
    r = np.clip(r_pos - r_neg, -3, 3)
    #r = 0
    #for i in range(len(joint_angles)):
    #    if i == 10 or i == 13:
    #        r -= (joint_angles[i]-1)**2
    #    else:
    #        r -= (joint_angles[i]-0)**2
    return r

def get_reward_jump(torso_pos, torso_quat, torso_vel, torso_angular_vel, joint_angles, joint_velocities, joint_torques,
               contacts, target_vel, max_steps):
    xd, yd, zd = torso_vel  # Unpack for clarity
    roll, pitch, yaw = p.getEulerFromQuaternion(torso_quat)

    # Calculate reward of the agent for traveling a self.target_vel velocity in the y direction (you can use your own proxy here!)
    # velocity_rew = 1. / (abs(yd - self.target_vel) + 1.) - 1. / (self.target_vel + 1.)
    #velocity_rew = 5*np.minimum(yd, target_vel) / target_vel + 2*np.minimum(zd, target_vel) / target_vel + 0.5*np.minimum(-zd, target_vel) / target_vel
    velocity_rew = np.minimum(yd, target_vel) / target_vel
     

    # Add various rewards here to your liking. For example, np.square(yaw) * 0.5 will penalise the robot facing directions other than the x direction
    # You can use other similar heuristics to 'guide' the agent into doing what you actually want it to do.
    r_neg = np.square(yaw) * 0.8 + \
            np.square(roll) * 0.3 + \
            np.square(pitch) * 0. + \
            np.square(zd) * 0.
    #r_neg = np.square(yaw) * 0.8 + \
    #        np.square(roll) * 0.3 + \
    #        np.square(pitch) * 0.1 + \
    #        np.square(zd) * 0.9
    #r_neg += 0.0001/(abs(sum(torso_vel))+1)
    r_pos = (velocity_rew * 1.0) / max_steps * 100 + np.square(pitch)*0.5 + np.square(zd)*0.1
    r = np.clip(r_pos - r_neg, -3, 3)
    return r
