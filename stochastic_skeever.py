import numpy as np
import os
from environment import Environment
import copy
import rev_mov as rm


# This script will run correctly assuming you have pythonpath pointing to the src directory. If you open a pycharm project inside the hw_1 file from sources then pythonpath should be set automatically
# Otherwise you can export PYTHONPATH to add the src directory in the terminal as follows: export PYTHONPATH=${PYTHONPATH}:/home/scripts/vir/hw_1



class PolicyNet():
    def __init__(self, obs_dim, act_dim):
        # Initialize your weight parameters of your policy.
        # The simplest policy to implement would be a simple affine mapping y = xw+b, where x is the input,
        # w is the weight matrix and b is the bias
        self.obs_dim = obs_dim
        self.act_dim = act_dim
        self.w = np.random.uniform(low=-1, high=1, size=(self.act_dim,self.obs_dim))/ np.sqrt(2)
        self.u = np.random.uniform(low=-1, high=1, size=(self.act_dim,1)) / np.sqrt(2)

        #self.d =  np.random.uniform(low=0.09, high=1, size=(self.act_dim,1))
        self.d = np.full((self.act_dim,1), 2*10**(0))

    def set_params(self, w):
        # This function takes in a list w and maps it to your policy parameters.
        # The simplest way is to probably make an array out of the w vector and reshape it appropriately
        # print(w.shape)
        [wm, u, d] = np.split(w, [(self.act_dim*self.obs_dim), (self.act_dim*self.obs_dim) + self.act_dim])
        self.w = np.reshape(wm, (self.act_dim, self.obs_dim))
        self.u = np.reshape(u, (self.act_dim,1))
        self.d = np.reshape(d, (self.act_dim,1))

    def get_params(self):
        # This function returns a list w from your policy parameters. You can use numpy's flatten() function
        vector = copy.deepcopy(self.w)
        vector.flatten()
        vector = np.append(vector, self.u)
        vector = np.append(vector, self.d)
        return vector

    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def grad_update(self,w,path,reward_path,action):
        p = np.reshape(path,(self.obs_dim,1))

        [w, u, d] = np.split(w, [(self.act_dim*self.obs_dim), (self.act_dim*self.obs_dim) + self.act_dim])
        #[w, u, d] = np.split(w, [(self.act_dim*self.obs_dim)])
        w = np.reshape(w, (self.act_dim, self.obs_dim))
        u = np.reshape(u, (self.act_dim,1))
        d = np.reshape(d, (self.act_dim,1))

        gw = copy.deepcopy(self.w)
        gw[:] = 0

        gu = copy.deepcopy(self.u)
        gu[:] = 0

        gd = copy.deepcopy(self.d)
        gd[:] = 0

        for i in range(self.act_dim):
            c = -2*((self.w[i,:] @ p + self.u[i]) - action[i]) / (self.d[i])
            gw[i,:] = c * path
            gu[i] = c * 1
            gd[i] = 0

        grad = copy.deepcopy(gw)
        grad.flatten()
        grad = np.append(grad, gu)
        grad = np.append(grad, gd)
        return grad


    def forward(self, x):
        # Performs the forward pass on your policy. Maps observation input x to action output a
        #print(f"w: {self.w}, u: {self.u}, d: {self.d}")
        #print(f"x: {x}")
        x = np.reshape(x,(self.obs_dim,1))
        out = []
        for i in range(self.act_dim):
            t = np.random.normal(self.w[i,:]@x + self.u[i], self.d[i])
            t = t[0]
            #if t>1:
            #    t = 1
            #elif t<-1:
            #    t = -1
            out.append(t)
        out = np.reshape(out,(self.act_dim))
        return out


def f_wrapper(env, policy):
    def f(w):
        reward = 0
        done = False
        obs = env.reset()
        obs = rm.reduce_redundant_obs(obs)
        # TODO: sfouknout obs o prebytecne klouby
        # Map the weight vector to your policy

        policy.set_params(w)
        reward_path = []
        path = []
        path.append(obs)
        actions = []
        while not done:
            # Get action from policy
            act = policy.forward(obs)

            #print(f"act: {act}")
            action = 2*policy.sigmoid(act/8) - np.ones(policy.act_dim)
            #print(f"action: {action}")
            action = rm.to_joint_array(action)
            # Step environment
            obs, rew, done, _ = env.step(action)
            obs = rm.reduce_redundant_obs(obs)
            
            path.append(copy.deepcopy(obs))
            reward_path.append(rew)
            actions.append(copy.deepcopy(act))
            reward += rew
        return reward, path, reward_path, actions

    return f


def my_opt(f, w_init, iters):
    # Your optimization algorithm. Takes in an evaluation function f, and initial solution guess w_init and returns
    # parameters w_best which 'solve' the problem to some degree.
    w = copy.deepcopy(w_init)
    alpha = 1*10**(-1)
    paths = []
    reward_paths = []
    rewards = []
    action_path = []
    avg_cnt = 50
    log_file = open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'training_log/stochastic_15_joints_s_walk.txt'),'w')
    w_best = copy.deepcopy(w)
    prew_crit_rew = np.NINF
    grad_o = np.zeros(w.shape)
    for i in range(iters):
        rewards = []
        paths = []
        reward_paths = []
        action_path = []
        rew_t = 0

        for t in range(avg_cnt):
            reward, path, reward_path, action = f(w)
            rewards.append(reward)
            paths.append(path)
            reward_paths.append(reward_path)
            action_path.append(action)
            rew_t += reward
            min_rew = min(rewards) 
        
        if alpha == 10**(-1) and rew_t/avg_cnt > 8:
            alpha == 10**(-2)

        if alpha == 10**(-2) and rew_t/avg_cnt > 30:
            alpha == 10**(-3)

        if rew_t/avg_cnt > 80:
            crit_rew =  rew_t
        else:
            crit_rew = rew_t
            
        grad = np.zeros(w.shape)
        for u in range(len(paths)):
            rew = rewards[u]
            path = paths[u]
            reward_path = reward_paths[u]
            action = action_path[u]
            for k in range(len(path)-1):
                grad += policy.grad_update(w,path[k],reward_path[k],action[k])*(rew-np.mean(rewards))/(np.std(rewards))

        grad *= alpha/(len(paths))
        print(f"max of grad: {max(abs(grad))}")
        print(f"rewards: min: {min(rewards)}, max: {max(rewards)}")
        w += grad
        log_file.write(f"{i}, {rew_t/avg_cnt};")
        print(f"Iteration no.{i}, reward: {rew_t/avg_cnt}")
        
        if i%1 == 0 and prew_crit_rew<crit_rew:
            np.save(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'skeevers/stochastic_15_joints_s_walk.npy'), w)
            prew_crit_rew = crit_rew
            w_best = copy.deepcopy(w)
            print("MODEL_SAVED")
        #if i != 0 and i%50==0:
        #    print(f"Avg test rew: {test(w_best, max_steps=30, animate=False)}")
    
    if prew_crit_rew<crit_rew:
        np.save(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'skeevers/stochastic_15_joints_s_walk.npy'), w)
        prew_crit_rew = crit_rew
        w_best = copy.deepcopy(w)
        print("MODEL_SAVED")
    return w_best, reward


def test(w_best, max_steps=70, animate=False):
    # Make the environment and your policy
    env = Environment(animate=animate, max_steps=max_steps)
    obs_len, joints_cnt = rm.get_param_dim()
    policy = PolicyNet(obs_len, joints_cnt)
    # Make evaluation function
    f = f_wrapper(env, policy)
    # Evaluate
    #w_best[-8:-1] = 0.00001
    #w_best[-1] = 0.00001
    r_avg = 0
    eval_iters = 10
    for i in range(eval_iters):
        r, _, _, _ = f(w_best)
        r_avg += r
        print(r)
    return r_avg / eval_iters


if __name__ == "__main__":
    #train = True
    train = False
    policy_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'skeevers/stochastic_15_joints_s_walk.npy')
    max_steps = 70
    N_training_iters = 4000
    w_best = None

    obs_len, joints_cnt = rm.get_param_dim()

    if train:
        # Make the environment and your policy
        env = Environment(animate=False, max_steps=max_steps)
        policy = PolicyNet(obs_len, joints_cnt)
        #obs dim = act dim + 8
        # Make evaluation function
        f = f_wrapper(env, policy)

        # Initial guess of solution
        #w_init = np.load(policy_path)
        w_init = policy.get_params()

        # Perform optimization
        w_best, r_best = my_opt(f, w_init, N_training_iters)

        print(f"final reward: {r_best}")

        # Save policy
        #np.save(policy_path , w_best)
        env.close()

    if not train:
        w_best = np.load(policy_path)
    print(f"Avg test rew: {test(w_best, max_steps=max_steps, animate=not train)}")



