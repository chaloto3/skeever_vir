import numpy as np
import os
# from src.Lab_2.quadruped.quadruped import QuadrupedBulletEnv
from environment import Environment
import copy
import rev_mov as rm


# This script will run correctly assuming you have pythonpath pointing to the src directory. If you open a pycharm project inside the hw_1 file from sources then pythonpath should be set automatically
# Otherwise you can export PYTHONPATH to add the src directory in the terminal as follows: export PYTHONPATH=${PYTHONPATH}:/home/scripts/vir/hw_1

def fit(points, rewards):
    size = points.shape
    dim = size[0]
    cnt = size[1]
    A = np.ones((cnt, dim))
    for i in range(dim):
        A[:, i] = points[i, :]
    #print(A.shape)
    B = rewards
    #Ainv = np.linalg.inv(A.transpose() @ A)
    #params = Ainv @ A.T @ B
    params = np.linalg.pinv(A) @ B
    return params/np.linalg.norm(params)


class PolicyNet():
    def __init__(self, obs_dim, act_dim):
        # Initialize your weight parameters of your policy.
        # The simplest policy to implement would be a simple affine mapping y = xw+b, where x is the input,
        # w is the weight matrix and b is the bias
        self.obs_dim = obs_dim
        self.act_dim = act_dim
        #self.act_dim = act_dim
        self.w = np.random.uniform(low=-2., high=2., size=(act_dim, obs_dim)) / np.sqrt(2)
        self.b = np.random.uniform(low=-2., high=2., size=act_dim) / np.sqrt(2)

    def set_params(self, w):
        # This function takes in a list w and maps it to your policy parameters.
        # The simplest way is to probably make an array out of the w vector and reshape it appropriately
        # print(w.shape)
        [wm, b] = np.split(w, [(self.act_dim * self.obs_dim)])
        self.w = np.reshape(wm, (self.act_dim, self.obs_dim))
        self.b = b

    def get_params(self):
        # This function returns a list w from your policy parameters. You can use numpy's flatten() function
        vector = copy.deepcopy(self.w)
        vector.flatten()
        vector = np.append(vector, self.b)
        # print(vector.shape)
        return vector

    def sigmoid_func(self, x):
        return 1 / (1 + np.exp(-x))

    def forward(self, x):
        # Performs the forward pass on your policy. Maps observation input x to action output a
        # x = np.reshape(x, (-1, 1))
        out = 2 * self.sigmoid_func((self.w @ x + self.b) / 32) - np.ones(self.act_dim)
        # print(out)
        return out


def f_wrapper(env, policy):
    def f(w):
        reward = 0
        done = False
        obs = env.reset()
        obs = rm.reduce_redundant_obs(obs)
        # TODO: sfouknout obs o prebytecne klouby
        # Map the weight vector to your policy

        policy.set_params(w)

        while not done:
            # Get action from policy
            act = policy.forward(obs)
            act = rm.to_joint_array(act)
            # TODO: nafouknout act o prebytecne klouby
            # Step environment
            obs, rew, done, _ = env.step(act)
            obs = rm.reduce_redundant_obs(obs)

            reward += rew
        return reward

    return f


def my_opt(f, w_init, iters):
    # Your optimization algorithm. Takes in an evaluation function f, and initial solution guess w_init and returns
    # parameters w_best which 'solve' the problem to some degree.
    log_file = open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'training_log/pseudograd_15joints.txt'),
                    'w')
    eps = 1e-3
    avg_cnt = 5
    point_cnt = 15
    alpha = 1e-2  #learning rate
    w_best = copy.deepcopy(w_init)
    w_shape = w_best.shape
    r_rand = 0
    r_best = 0
    for i in range(avg_cnt):
        r_best += f(w_best)
    r_best = r_best / avg_cnt
    rwrds = np.zeros(point_cnt)
    data = np.zeros((point_cnt, w_shape[0]))
    print(f"rwrds shape: {rwrds.shape}; data shape: {data.shape}")
    for i in range(iters):
        for k in range(point_cnt):
            new_w = np.random.uniform(low=-eps, high=eps, size=w_shape)
            new_w += w_best
            data[k, :] = new_w
            new_r = 0
            for j in range(avg_cnt):
                new_r += f(new_w)
            new_r = new_r / avg_cnt
            rwrds[k] = new_r
            if new_r > r_best:
                r_rand = new_r
                w_rand = copy.deepcopy(new_w)
        #grad = fit(data, rwrds)
        #grad = np.linalg.pinv(data) @ rwrds

        lamb = 1e-2
        col = data.shape[1]
        grad = np.linalg.lstsq(data.T.dot(data) + lamb * np.identity(col), data.T.dot(rwrds))[0]
        #print(grad)

        tmp_w = w_best + alpha*grad
        tmp_r = 0
        for j in range(avg_cnt):
            tmp_r += f(tmp_w)
        tmp_r = tmp_r / avg_cnt
        #if tmp_r > r_best:
        w_best = tmp_w
        r_best = tmp_r
        print("------------------------------------------------------")
        print(f"Iteration no.{i} best reward: {r_best};\n random max search reward: {r_rand}")
        log_file.write(f"{i}, {r_best};")
        np.save(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'skeevers/pseudogradient_15joints.npy'), w_best)
    return w_best, r_best


def test(w_best, max_steps=70, animate=False):
    # Make the environment and your policy
    env = Environment(animate=animate, max_steps=max_steps)
    obs_len, joints_cnt = rm.get_param_dim()
    policy = PolicyNet(obs_len, joints_cnt)

    # Make evaluation function
    f = f_wrapper(env, policy)

    # Evaluate
    r_avg = 0
    eval_iters = 10
    for i in range(eval_iters):
        r = f(w_best)
        r_avg += r
        print(r)
    return r_avg / eval_iters


if __name__ == "__main__":
    train = True
    #train = False
    policy_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'skeevers/pseudogradient_15joints.npy')
    max_steps = 70
    N_training_iters = 5000
    w_best = None

    obs_len, joints_cnt = rm.get_param_dim()

    if train:
        # Make the environment and your policy
        env = Environment(animate=False, max_steps=max_steps)
        policy = PolicyNet(obs_len, joints_cnt)
        #obs dim = act dim + 8
        # Make evaluation function
        f = f_wrapper(env, policy)

        # Initial guess of solution
        w_init = policy.get_params()

        # Perform optimization
        w_best, r_best = my_opt(f, w_init, N_training_iters)

        print(f"r_best: {r_best}")

        # Save policy
        np.save(policy_path , w_best)
        env.close()

    if not train:
        w_best = np.load(policy_path)
    print(f"Avg test rew: {test(w_best, max_steps=max_steps, animate=not train)}")



