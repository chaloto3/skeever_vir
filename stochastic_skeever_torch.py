import numpy as np
import os
from environment import Environment
import copy
import only8joints as rm

import torch as T
import torch.nn as NN
import torch.optim as optim

class PolicyNet(NN.Module):
    def __init__(self, obs_dim, act_dim):
        super().__init__()
        self.obs_dim = obs_dim
        self.act_dim = act_dim
        # define neural network
        self.network1 = NN.Sequential(
            NN.Linear(self.obs_dim, self.obs_dim*4),
            NN.Linear(self.obs_dim*4, self.obs_dim*2),
            NN.Linear(self.obs_dim*2, 1),
        )

        self.network2 = NN.Sequential(
            NN.Linear(self.obs_dim, self.obs_dim*4),
            NN.Linear(self.obs_dim*4, self.obs_dim*2),
            NN.Linear(self.obs_dim*2, 1),
        )

        self.network3 = NN.Sequential(
            NN.Linear(self.obs_dim, self.obs_dim*4),
            NN.Linear(self.obs_dim*4, self.obs_dim*2),
            NN.Linear(self.obs_dim*2, 1),
        )
        
        self.network4 = NN.Sequential(
            NN.Linear(self.obs_dim, self.obs_dim*4),
            NN.Linear(self.obs_dim*4, self.obs_dim*2),
            NN.Linear(self.obs_dim*2, 1),
        )
        
        self.network5 = NN.Sequential(
            NN.Linear(self.obs_dim, self.obs_dim*4),
            NN.Linear(self.obs_dim*4, self.obs_dim*2),
            NN.Linear(self.obs_dim*2, 1),
        )

        self.network6 = NN.Sequential(
            NN.Linear(self.obs_dim, self.obs_dim*4),
            NN.Linear(self.obs_dim*4, self.obs_dim*2),
            NN.Linear(self.obs_dim*2, 1),
        )

        self.network7 = NN.Sequential(
            NN.Linear(self.obs_dim, self.obs_dim*4),
            NN.Linear(self.obs_dim*4, self.obs_dim*2),
            NN.Linear(self.obs_dim*2, self.act_dim),
        )

        self.network8 = NN.Sequential(
            NN.Linear(self.obs_dim, self.obs_dim*4),
            NN.Linear(self.obs_dim*4, self.obs_dim*2),
            NN.Linear(self.obs_dim*2, self.act_dim),
        )

    def forward(self, x):
        out1 = self.network1(T.FloatTensor(x))
        out2 = self.network2(T.FloatTensor(x))
        out3 = self.network3(T.FloatTensor(x))
        out4 = self.network4(T.FloatTensor(x))
        out5 = self.network5(T.FloatTensor(x))
        out6 = self.network6(T.FloatTensor(x))
        out7 = self.network7(T.FloatTensor(x))
        out8 = self.network8(T.FloatTensor(x))
        return out1[0],out2[0],out3[0],out4[0],out5[0],out6[0],out7[0],out8[0]


def f_wrapper(env, policy):
    def f():
        reward = 0
        done = False
        obs = env.reset()
        obs = rm.reduce_redundant_obs(obs)
        # TODO: sfouknout obs o prebytecne klouby
        # Map the weight vector to your policy

        reward_path = []
        path = []
        actions = []
        grad = 0
        while not done:
            # Get action from policy
            obs = np.reshape(obs, (1,policy.obs_dim))
            obs = T.tensor(obs)
            act = policy(obs)

            print(f"action: {act}")
            #action[int(act//policy.act_disc)] = policy.acts[int(act)]
            #print(action)
            with T.no_grad():
                action = rm.to_joint_array(np.array(act[0]))
                # Step environment
                obs, rew, done, _ = env.step(action)
                obs = rm.reduce_redundant_obs(obs)

            act = T.log(act)
            
            act.backward()   
            grad += act.grad
            
            act.zero_grad()
            
            path.append(obs)
            reward_path.append(rew)
            actions.append(action)
            reward += rew
        
        return reward, path, reward_path, grad

    return f


def learn(f, iters):
    # parameters w_best which 'solve' the problem to some degree.
    alpha = 10**(-6)
    paths = []
    reward_paths = []
    rewards = []
    action_path = []
    avg_cnt = 30
    log_file = open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'training_log/stochastic_8_joints.txt'),'w')
    

    for i in range(iters):
        rew_t = 0
        rewards = []
        

        for t in range(avg_cnt):
            reward = 0
            done = False
            obs = env.reset()
            obs = rm.reduce_redundant_obs(obs)

            
            path = [obs]
            actions = []
            grad = 0
            while not done:
                # Get action from policy
                obs = np.reshape(obs, (1,policy.obs_dim))
                obs_T = T.tensor(obs)
                act1,act2,act3,act4,act5,act6,act7,act8 = policy(obs_T)
                act = [act1[0].detach().numpy(),act2[0].detach().numpy(),act3[0].detach().numpy(),act4[0].detach().numpy(),act5[0].detach().numpy(),act6[0].detach().numpy(),act7[0].detach().numpy(),act8[0].detach().numpy()] 
                print(f"act: {act}")
                action = rm.to_joint_array(act)
                # Step environment
                obs, rew, done, _ = env.step(action)
                obs = rm.reduce_redundant_obs(obs)
                
                actions.append(act)
                path.append(obs)


            rew_t += rew
        
            #for p in policy.parameters():
            #    p.data.sub_(
            

        log_file.write(f"{i}, {rew_t/avg_cnt};")
        print(f"Iteration no.{i}, reward: {rew_t/avg_cnt}")

        #if i%10 == 0:
        #    np.save(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'skeevers/stochastic_8_joints.npy'), w)
    
    return w, reward












def test(w_best, max_steps=70, animate=False):
    # Make the environment and your policy
    env = Environment(animate=animate, max_steps=max_steps)
    obs_len, joints_cnt = rm.get_param_dim()
    policy = PolicyNet(obs_len, joints_cnt)

    # Make evaluation function
    f = f_wrapper(env, policy)
    
    # Evaluate
    r_avg = 0
    eval_iters = 10
    for i in range(eval_iters):
        r, _, _, _ = f(w_best)
        r_avg += r
        print(r)
    return r_avg / eval_iters


if __name__ == "__main__":
    train = True
    #train = False
    policy_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'skeevers/stochastic_torch_8_joints.npy')
    max_steps = 50
    N_training_iters = 50
    w_best = None

    obs_len, joints_cnt = rm.get_param_dim()

    if train:
        # Make the environment and your policy
        env = Environment(animate=True, max_steps=max_steps)
        policy = PolicyNet(obs_len, joints_cnt)
        #obs dim = act dim + 8
        # Make evaluation function
        f = f_wrapper(env, policy)

        # Perform optimization
        w_best, r_best = learn(f, N_training_iters)

        print(f"final reward: {r_best}")

        # Save policy
        np.save(policy_path , w_best)
        env.close()

    if not train:
        w_best = np.load(policy_path)
    print(f"Avg test rew: {test(w_best, max_steps=max_steps, animate=not train)}")
