import numpy as np

def reduce_redundant_obs(obs):
    """reduces redundant information about static joints"""
    # To convert joint values from skeever to network
    # Edit to suit your needs into different file
    # Input order: 19 joint values, 4 quaternions, 4 contacts
    # Output order: Flegs (3 joints), hip, Blegs(4 joints), 4 quaternions, contacts

    return np.concatenate((obs[9:12], [obs[8]], obs[1:5], obs[19:])).astype(np.float32)
    # return obs[9:15] + [obs[8]] + obs[1:9] + [obs[17]] + obs[19:]


def to_joint_array(joints):
    '''joint values from network to joint array'''
    # To convert joint values generated by network to joint array of skeever
    # Edit to suit your needs into different file
    # Order: Torso_to_base_link, BRleg (4 joints), BLleg (4 joints), hip, FRleg (3 joints), FLleg (3 joints), head_to_torso, Jaw, tail_to_torso

    return np.concatenate(
        ([0], joints[4:8], joints[4:8], [joints[3]], joints[0:3], joints[0:3], [0], [0], [0])).astype(
        np.float32)

def get_param_dim():
    joints_cnt = 8
    observation = joints_cnt + 8
    return observation, joints_cnt