# Skeever_VIR

Skeever from Skyrim will be taught to attack foe on sight.
edit: Skeever from Skyrim will run as fast as he can
**trained skeevers:**
- stochastic_8_joints_s_walk.npy - walking reward function stochastic skeever
- random_search_8_joints_walk.npy - walking reward function random search algorithm
- pseudogradient_8joints.npy - doesn't work but I would film it anyway

- stochastic_8_joints_s_jump2.npy - latest jumping function (does not jump so much...)
- random_search_8_joints_jump2.npy - latest jumping function (does not jump so much...)

- stochastic_8_joints_s_dance_of_joy.npy - so weird jmping function (I would film version with and without lowaring randomizataion)

**Notes:**
- pls delete 8joints.py


**Structure of this project**
- environment.py - main module, edit only if you're editing skeever model or switching reward function
- reward_funcs.py - module dedicated to hold all different reward functions, imported by environment.py
- rev_mov.py - template for reward function and pack/unpack observations and action space 
- only8joints.py - module with pack/unpack functions, reduces the joints to 3 front legs joints, spine joint and 4 back leg joints, basically both front/back legs move the same way
- blackbox_opt_ala_hw1.py - random search nn, exactly the same as in VIR HW1

**See also:**
(1) DDPG:

https://arxiv.org/abs/1509.02971

Existuje spousta vysvetleni a jiz hotovych implementaci (idealne se inspirujte ale nedelal by slepy copy-paste):
https://towardsdatascience.com/deep-deterministic-policy-gradients-explained-2d94655a9b7b
https://github.com/tobiassteidle/Reinforcement-Learning/tree/master/OpenAI/MountainCarContinuous-v0


(2) Pokud se povede muzete zkusit jeste jedno vylepseni TD3:
https://spinningup.openai.com/en/latest/algorithms/td3.html


(3) DQN je implementacne i mentalne nejjednodussi, ale je potreba dobre rozmyslet diskretizaci action-space, protoze vystupem site jsou Q-hodnoty pro vsechny mozne kombinace akci.
Napr. zjednodusim:
 - obe zadni nohy i obe predni nohy delaji pri skoku to same + mam jen 2 motory na nohu => 4 dimensionalni rizeni.
 - pokud rozlisuji treba jen 3 diskretni stavy na dimenzy (+1…dopredu, 0… uprostred, -1…zpet) tak dostanu 3^4=81 vystupu, coz je asi pouzitelne.
https://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html

**some music to listen to while developing**

https://www.youtube.com/playlist?list=PLFKKmQgdWNIwN9-z1mUUJsHc5fxRiD69V
